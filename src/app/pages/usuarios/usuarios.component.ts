import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/entities/usuario';
import { UsuariosService } from '../../services/usuarios.service';
import { Router } from '@angular/router';

declare const Swal;
declare const jQuery;

@Component({
    selector: 'app-usuarios',
    templateUrl: './usuarios.component.html',
    styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {


    usuarios: Array<Usuario>;
    usuario: Usuario;
    generos: Array<any>;
    pageActualEmpresas: number = 1;
    esEdicion: number
    nombreUsuarioEditar: string


    constructor(private usuariosService: UsuariosService, private router: Router) {
        this.usuarios = [];
        this.usuario = new Usuario();
        this.generos = [
            'Masculino',
            'Femenino'
        ]
        
    }
    ngOnInit() {
        this.load();
        console.log(this.usuarios);
    }

    /**
     * Permite traer todo la informacion de los usuarios
     */
    private load(){
        this.usuariosService.getAll().subscribe((response: Array<Usuario>) => {
            this.usuarios = response['usuarios'];
        })
    }
    /**
     * Permite elimininar un usuario
     * @param _id 
     * @param i 
     */
    public remove(_id: string, i: number){
        Swal({
            title: 'Está seguro que desea eliminar esta usuario?',
            showConfirmButton: true,
            showCancelButton: true,
            type: 'question'
        }).then(succ => {
            if (succ.value) {
                // Se llama al servicio para eliminar el usuario
                this.usuariosService.remove(_id)
                    .subscribe(succ => {
                        // una vez se elimina el usuario, se quita del array de usuarios
                        this.usuarios.splice(i, 1);
                        Swal({
                            title: 'Se eliminó correctamente',
                            showConfirmButton: false,
                            timer: 1000
                        });
                        this.load();
                    });
            }
        });
    }

    /**
     * Permite abrir el modal de registro de usuario
     */
    public abrirFormUsuario(idUsuario: any){
        this.esEdicion = idUsuario;

        for(let i = 0; i <this.usuarios.length; i++){
            if(this.usuarios[i].id == idUsuario){
                this.usuario.id = this.usuarios[i].id;
                this.usuario.name = this.usuarios[i].name;
                this.usuario.email = this.usuarios[i].email;
                this.usuario.age = this.usuarios[i].age;
                this.usuario.gender = this.usuarios[i].gender;
                this.usuario.password = this.usuarios[i].password;
            }
        }

        jQuery('#registrarUsuario').show();
    }
    /**
     * Permite cerrar la modal de registro de usuario
     */
    public cerrarFormUsuario(){
        jQuery('#registrarUsuario').hide();

        this.usuario.id = null;
        this.usuario.name = null;
        this.usuario.email = null;
        this.usuario.age = null;
        this.usuario.gender = null;
        this.usuario.password = null;
    }
    
    /**
     * Permite registrar un nuevo usuario
     */
    public save(idUsuario: number){
        // Si es mayor a cero es porque se debe crear un usuario
        if(idUsuario == 0){
            // Se hace la petición para guardar el usuario
            this.usuariosService.save(this.usuario).subscribe(
                data => {
                    Swal.fire({
                        icon: 'success',
                        title: data['mensaje'],
                        showConfirmButton: false,
                        timer: 1000
                    });

                    this.load();
                },
                error => {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: `Ya existe un usuario registrado con el email ${this.usuario.email}`,
                    });
                }
            )
        }
    }
}
