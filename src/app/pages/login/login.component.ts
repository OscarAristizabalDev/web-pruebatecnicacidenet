import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../../entities/usuario';
import { AuthService } from '../../services/auth.service';

declare var jQuery;
declare const Swal;
//import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    formData: Usuario;

    constructor(private authService: AuthService, private router: Router) {
        this.formData = new Usuario();
    }

    ngOnInit(): void {
    }

    /**
     * Permite hacer el login del usuario
     */
    async login(){
        jQuery.fancybox.showLoading();
        // Se hace la peticion al metodo login, se envia el usuario que se autentica
        const userData = await this.authService.login(this.formData);
        // verificamos si la consulta devolvio algun registro
        const response = JSON.stringify(userData);

        console.log(`Usuario:  ${response}`);
        if(userData == null){

            jQuery.fancybox.hideLoading();
            Swal({
                type: 'error',
                title: 'Oops...',
                text: 'Usuario o contraseña incorrecta!'
            })
        }else{
            jQuery.fancybox.hideLoading();
            this.router.navigate(['/usuarios']);
        }
        jQuery.fancybox.hideLoading();
    }

}
