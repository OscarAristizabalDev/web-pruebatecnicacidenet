import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuario } from 'src/app/entities/usuario';

declare var jQuery;
declare const Swal;

@Component({
  selector: 'app-form-usuario',
  templateUrl: './form-usuario.component.html',
  styleUrls: ['./form-usuario.component.scss']
})
export class FormUsuarioComponent implements OnInit {

    formData: Usuario;
    generos: Array<any>;

    constructor(
        private usuariosService: UsuariosService, 
        private router: Router,
        private currentRoute: ActivatedRoute
    ) {
        this.generos = [
            'Masculino',
            'Femenino'
        ]
        this.formData = new Usuario();

        this.currentRoute.params.subscribe(params => {
            if(typeof params.id !== 'undefined'){
                this.load(params.id);
            }
        })
    }

    ngOnInit(): void {
    
    }

    /**
     * Permite consultar en usuario por ID
     * @param _id 
     */
    private load(_id: string){
        this.usuariosService.getById(_id)
            .subscribe((response: Usuario) => {
                this.formData = response;
            })
    }

     /**
     * Funcion que permita registrar un usuario
     */
    public save() {
        jQuery.fancybox.showLoading();

        /**this.usuariosService.save(this.formData).subscribe(
            (Response) => {
                jQuery.fancybox.hideLoading(); 
            },
            (responseError) => {
                jQuery.fancybox.hideLoading();
            }
        );*/
    }

}
