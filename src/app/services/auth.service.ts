import { HttpClient } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../entities/usuario';

declare var jQuery;

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    public token: string;
    public userLogged: Usuario

    public loginEvent: EventEmitter<Usuario>;
    public logoutEvent: EventEmitter<void>;

    constructor(private http: HttpClient) {
        this.token = null;
        this.userLogged = null;

        //creamos los disparadores de eventos
        this.loginEvent = new EventEmitter<Usuario>();
        this.logoutEvent = new EventEmitter<void>();

        this.load();
    }

    /**
     * Permite cargar la información del usuario logeado
     */
    public load() {
        this.token = localStorage.getItem('token');
        const userLogged = localStorage.getItem('userLogged');
        if (userLogged !== null) {
            this.userLogged = JSON.parse(userLogged);
        }
    }

    /**
     * Permite guardar la información del usuario logeado
     */
    private save() {
        if (this.token !== null) {
            localStorage.setItem('token', this.token);
        }

        if (this.userLogged !== null) {
            localStorage.setItem('userLogged', JSON.stringify(this.userLogged));
        }
    }
    /**
     * Permite enviar al servidor las credenciales de acceso 
     * para que un usuario pueda iniciar sesión
     * @param authForm 
     */
    login(authForm: Usuario): Promise<Usuario> {
        return new Promise((resolve) => {
            this.http.post(environment.apiUrl.login, authForm)
                .subscribe((response) => {
                    this.token = response['token'];

                    console.info('token'+ this.token);

                    this.userLogged = new Usuario();
                    this.userLogged.id = response['usuario'].id;
                    this.userLogged.name = response['usuario'].name;
                    this.userLogged.email = response['usuario'].email;
                    // guardamos la información por si el ususario refresca la pestaña del navegador (recargar pagina)
                    this.save();

                    resolve(this.userLogged);
                    
                    this.loginEvent.emit(this.userLogged);

                    setTimeout(() => {
                        jQuery(window).trigger('load');
                    }, 1000);

                }, (responseError) => {
                    console.info("responseError", responseError);
                    resolve(null);
                });
        });
    }

    /**
     * Permite cerrar sesión
     */
    closeSession() {
        this.userLogged = null;
        this.token = null;
        this.remove();
        this.logoutEvent.emit();
    }
    /**
     * Permite borrar la información almacenada localmente
     */
    private remove() {
        localStorage.removeItem('token');
        localStorage.removeItem('userLogged');
    }

    /**
     * Permite cerrar la sesión del usuario
     */
    logout() {
        this.http.post(environment.apiUrl.logout, null)
            .subscribe(() => {
                this.closeSession();
            })
    }
}
