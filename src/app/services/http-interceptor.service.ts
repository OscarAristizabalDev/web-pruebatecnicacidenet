import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

declare var jQuery;


@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

    constructor(private injector: Injector) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Accedemos a la información del usuario autenticado
        const authService = this.injector.get(AuthService);
        
        let request = req;
        // Se verifica si exite un token
        if (authService.token) {
            // Se envia el token por el header
            request = req.clone({
                setHeaders: {
                    'Content-Type': 'application/json',
                    'authorization': authService.token,
                }
            });
        }

        return next.handle(request)
            .pipe(
                tap(
                    event => {
                        // Si se cuenta con los permisos para acceder a la inf
                        if (event instanceof HttpResponse) {
                            // console.log('api call success : ', event);
                        }
                    },
                    error => {
                        if (event instanceof HttpResponse) {
                            
                        } else {
                            
                        // En caso de presentarse un error de authorizacione
                            if (error.status === 498 || error.status === 499 || error.status === 401) {
                                
                                const router = this.injector.get(Router);
                                // Se cierra la cesión
                                authService.closeSession();
                                jQuery.fancybox.hideLoading();
                                // Se redirecciona al login
                                router.navigate(['/login']);
                            }
                        }
                    }
                )
            );
    }
}
