import { HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Usuario } from '../entities/usuario';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

    authService;

    constructor(private http: HttpClient, private injector: Injector) { 
        this.authService = this.injector.get(AuthService);
    }

    /**
     * Permite obtener todos los registros de usuarios
     */
    //public async getAll() {
        
    public getAll() {
        
        return this.http.get(environment.apiUrl.usuarios);

        /**try {
            // Se consultan los usuarios registrados
            const res = await fetch(environment.apiUrl.usuarios , {
                headers:{
                    'Content-Type':'application/json',
                    'Authorization': this.authService.token
                }  
            })
            // Se por algún motivo no tiene autorización
            if(res.status == 401){
                // Se redirecciona al login
                const router = this.injector.get(Router);
                router.navigate(['/login']);
            }

            // Se retorna los usuarios 
            const usuarios = await res.json();
            return usuarios;

        } catch (error) {
            
        }*/
    }

    /**
     * Permite obtener todas las cuentas de una empresa
     * @param _id 
     */
    public getCuentasByEmpresa(_id: string){
        return this.http.get(environment.apiUrl.usuarios + '/empresas/' + _id + '/cuentas');
    }

    
    /**
     * permite obtener un registro por ID
     * @param id 
     */
    public getById(id: string) {
        return this.http.get(environment.apiUrl.usuarios + '/detail/' + id);
    }

    /**
     * Permite guardar los datos de un registro
     * @param formData 
     */
    public save(formData: Usuario){
         // si no hay registro en id es por que se va a registrar   
        if(typeof formData.id === 'undefined' || formData.id === null){
            // como vamos a registrar un usuario, eliminamos el id del formulario
            delete formData.id;
            return this.add(formData);
        }
        return this.edit(formData);
    }

    /**
     * Permite registrar los datos de una cuenta
     * @param formData 
     */
    public add(formData: Usuario){
        return this.http.post(environment.apiUrl.usuarios + '/create', formData);
    }

    /**
     * Permite editar los datos de un registro
     * @param formData 
     */
    public edit(formData: Usuario){
        return this.http.put(environment.apiUrl.usuarios + '/cuentas/' + formData.id , formData);
    }

    /**
     * permite eliminar una cuenta
     * @param _id 
     */
    public remove(_id: string){
        return this.http.delete(environment.apiUrl.usuarios + '/delete/'+ _id); 
    }
}
