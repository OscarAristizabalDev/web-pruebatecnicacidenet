import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { UsuariosComponent } from './pages/usuarios/usuarios.component';
import { FormUsuarioComponent } from './pages/form-usuario/form-usuario.component';


const routes: Routes = [

    {path: '', component: LoginComponent},
    {path: 'usuarios', component: UsuariosComponent},
    {path: 'create-usuario', component: FormUsuarioComponent}
    

];

@NgModule({
    imports:  [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
