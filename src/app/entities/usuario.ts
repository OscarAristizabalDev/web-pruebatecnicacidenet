export class Usuario {

    id: string;
    name: string;
    email: string;
    age: number;
    gender: string;
    password: string;
    active: boolean;

    constructor(){
        this.id = null;
        this.name = null;
        this.email = null;
        this.age = null;
        this.gender = null;
        this.active = null;
    }
}
