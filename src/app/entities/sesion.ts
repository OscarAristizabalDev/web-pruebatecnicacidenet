export class Sesion {
    id: string;
    token: string;
    expired_at: string;
    usuario_id: number;
    
    constructor(){
        this.id = null;
        this.token = null;
        this.expired_at = null;
        this.usuario_id = null;
    }
}
